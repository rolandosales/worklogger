# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-06-07 00:37
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('work', '0003_log_day'),
    ]

    operations = [
        migrations.AlterField(
            model_name='log',
            name='project',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='work.Project'),
        ),
    ]
