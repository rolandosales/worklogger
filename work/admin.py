from django.contrib import admin
from .models import Project, Log


class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'total_duration')


admin.site.register(Project, ProjectAdmin)
admin.site.register(Log)
