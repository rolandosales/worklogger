from django.db import models
from django.db.models import Sum


class Project(models.Model):
    name = models.CharField(max_length=200)

    def total_duration(self):
        logs = Log.objects.filter(project=self)
        total = logs.aggregate(Sum("duration"))["duration__sum"]
        return total

    def __str__(self):
        return self.name


class Log(models.Model):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    duration = models.DurationField()
    project = models.ForeignKey('Project', null=True,
                                on_delete=models.SET_NULL)
    remarks = models.TextField()
    day = models.DateField(default=None)

    def __str__(self):
        string = str(self.duration)
        string += "\t"
        string += str(self.project)
        string += "\t"
        string += self.remarks
        return string
