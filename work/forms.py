from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Log


class UserCreateForm(UserCreationForm):

    class Meta:
        model = User
        fields = ("username", "password1", "password2")

    def save(self, commit=True):
        user = super(UserCreateForm, self).save(commit=False)
        if commit:
            user.save()
        return user


class LogForm(forms.ModelForm):

    class Meta:
        model = Log
        fields = ('duration', 'project', 'remarks', 'day')


class DateForm(forms.Form):
    day = forms.DateField()
