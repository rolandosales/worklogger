from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, authenticate
from django.db.models import Sum
from forms import UserCreateForm, LogForm, DateForm
from work.models import Log
import datetime


def home(request):

    if request.user.is_authenticated():
        datetoday = datetime.date.today()
        day_logs = Log.objects.filter(user=request.user,
                                      day=datetoday)
        month_logs = Log.objects.filter(user=request.user,
                                        day__year=datetoday.year,
                                        day__month=datetoday.month)

        start_week = datetoday
        start_week -= datetime.timedelta(datetoday.weekday())
        end_week = start_week + datetime.timedelta(7)

        week_logs = Log.objects.filter(user=request.user,
                                       day__range=[start_week, end_week])

        total_day = day_logs.aggregate(Sum("duration"))["duration__sum"]
        total_month = month_logs.aggregate(Sum("duration"))["duration__sum"]
        total_week = week_logs.aggregate(Sum("duration"))["duration__sum"]
        if request.method == 'POST':
            form = LogForm(request.POST)
            if form.is_valid():
                log = form.save(commit=False)
                log.user = request.user
                log.save()
                total_day = day_logs.aggregate(Sum(
                    "duration"))["duration__sum"]
                total_month = month_logs.aggregate(Sum(
                    "duration"))["duration__sum"]
                total_week = week_logs.aggregate(Sum(
                    "duration"))["duration__sum"]
                return render(request, 'work/home.html',
                              {'user': request.user, 'form': LogForm(),
                               'day_logs': day_logs, 'total_day': total_day,
                               'month_logs': month_logs,
                               'total_month': total_month,
                               'week_logs': week_logs,
                               'total_week': total_week,
                               'display_week': total_week is not None,
                               'display_day': total_day is not None,
                               'display_month': total_month is not None})
            else:
                return render(request, 'work/home.html',
                              {'user': request.user, 'form': form,
                               'day_logs': day_logs, 'total_day': total_day,
                               'month_logs': month_logs,
                               'total_month': total_month,
                               'week_logs': week_logs,
                               'total_week': total_week,
                               'display_week': total_week is not None,
                               'display_day': total_day is not None,
                               'display_month': total_month is not None})
        else:
            form = LogForm()
        return render(request, 'work/home.html',
                      {'user': request.user, 'form': form,
                       'day_logs': day_logs, 'total_day': total_day,
                       'month_logs': month_logs, 'total_month': total_month,
                       'display_day': total_day is not None,
                       'week_logs': week_logs,
                       'total_week': total_week,
                       'display_week': total_week is not None,
                       'display_month': total_month is not None})
    else:
        if request.method == 'POST':
            form = AuthenticationForm(None, request.POST)
            if form.is_valid():
                username = form.cleaned_data.get('username')
                raw_password = form.cleaned_data.get('password')
                user = authenticate(username=username, password=raw_password)
                login(request, user)
                return redirect('home')
        else:
            form = AuthenticationForm()
        return render(request, 'registration/login.html', {'form': form})


def signup(request):
    if request.method == 'POST':
        form = UserCreateForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = UserCreateForm()
    return render(request, 'registration/signup.html', {'form': form})


def viewlogs(request):
    if request.method == 'POST':
        form = DateForm(request.POST)
        if form.is_valid():
            day = form.cleaned_data.get('day')
            day_logs = Log.objects.filter(user=request.user, day=day)
            month_logs = Log.objects.filter(
                user=request.user,
                day__year=day.year,
                day__month=day.month)
            start_week = day
            start_week -= datetime.timedelta(day.weekday())
            end_week = start_week + datetime.timedelta(7)

            week_logs = Log.objects.filter(user=request.user,
                                           day__range=[start_week, end_week])
            total_day = day_logs.aggregate(Sum("duration"))["duration__sum"]
            total_month = month_logs.aggregate(
                Sum("duration"))["duration__sum"]
            total_week = week_logs.aggregate(Sum("duration"))["duration__sum"]
            return render(request, 'work/viewlogs.html',
                          {'form': form, 'day_logs': day_logs, 'day': day,
                           'total_day': total_day,
                           'display_day': total_day is not None,
                           'total_month': total_month,
                           'display_month': total_month is not None,
                           'week_logs': week_logs,
                           'total_week': total_week,
                           'display_week': total_week is not None,
                           'month_logs': month_logs})
    else:
        form = DateForm()
    return render(request, 'work/viewlogs.html',
                  {'form': form, 'display_day': False})
